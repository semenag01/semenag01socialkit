Pod::Spec.new do |s|
    
	#root
		s.name     = 'Semenag01SocialKit'
		s.summary  = 'Social API for FB, TW, G+, VK, OD'
		s.version  = '0.0.1'
		s.license   = { :type => 'Apache', :file => 'LICENSE.txt' }
	
		s.homepage = 'https://bitbucket.org/semenag01'
		s.author   = 'semenag01'
		s.source   = { :git => 'https://semenag01@bitbucket.org/semenag01/semenag01socialkit.git', :branch => 'master', :tag => '0.0.1' }
	
	#platform
		s.platform = :ios, '7.0' 
		s.ios.deployment_target = '7.0'
	
	#build settings 
	  	s.requires_arc = true
  
  	#file patterns
		# s.source_files = 'Semenag01SocialKit/**/*.{h,m}'
	
	#subspecs
		s.subspec 'Core' do |core|
			core.dependency 'Semenag01Kit'
			core.source_files = 'Semenag01SocialKit/Core/**/*.{h,m}'
	        core.resources = "Semenag01SocialKit/**/*.{jpeg,png,xib,nib,txt}"
	    end
		
		s.subspec 'Facebook' do |facebook|
			facebook.dependency 'Semenag01SocialKit/Core'
            facebook.dependency 'Facebook-iOS-SDK'
            facebook.source_files = 'Semenag01SocialKit/Facebook/**/*.{h,m}'
		end

        s.subspec 'Vkontakte' do |vkontakte|
            vkontakte.dependency 'Semenag01SocialKit/Core'
            vkontakte.dependency 'VK-ios-sdk'
            vkontakte.source_files = 'Semenag01SocialKit/Vkontakte/**/*.{h,m}'
        end
		s.subspec 'Twitter' do |twitter|
			twitter.dependency 'Semenag01SocialKit/Core'
			twitter.dependency 'STTwitter'
			twitter.source_files = 'Semenag01SocialKit/Twitter/**/*.{h,m}'
		end
		
		s.subspec 'Odnoklassniki' do |odnoklassniki|
			odnoklassniki.dependency 'Semenag01SocialKit/Core'
			odnoklassniki.dependency 'odnoklassniki_ios_sdk'
			odnoklassniki.source_files = 'Semenag01SocialKit/Odnoklassniki/**/*.{h,m}'
		end


        s.subspec 'GooglePlus' do |googleplus|
            googleplus.dependency 'Semenag01SocialKit/Core'
            googleplus.vendored_frameworks = 'Frameworks/GooglePlus.framework', 'Frameworks/GoogleOpenSource.framework'
            googleplus.resource = "Frameworks/GooglePlus.bundle"
            googleplus.framework = 'AssetsLibrary', 'CoreLocation', 'CoreMotion', 'CoreGraphics', 'CoreText', 'MediaPlayer', 'Security', 'SystemConfiguration', 'AddressBook'
            googleplus.xcconfig = { 'HEADER_SEARCH_PATHS' => '"${PODS_ROOT}/Frameworks/GoogleOpenSource.framework/Versions/A/Headers"' }

            googleplus.source_files = 'Semenag01SocialKit/GooglePlus/**/*.{h,m}'
        end

end

