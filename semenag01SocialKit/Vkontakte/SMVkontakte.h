//
//  SMVkontakte.h
//  Pro-Otdyh-New
//
//  Created by Yuriy Bosov on 11/13/13.
//  Copyright (c) 2013 semenag01. All rights reserved.
//
#import "SMSocialNetwork.h"

#define SMVkontakteInstance [SMVkontakte sharedInstance]

@interface SMVkontakte : SMSocialNetwork

SM_DECLARE_SINGLETON;

@end
