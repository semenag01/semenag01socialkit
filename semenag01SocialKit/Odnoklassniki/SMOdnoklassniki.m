//
//  SMOdnoklassniki.m
//  WebTaxi
//
//  Created by semenag01 on 12/3/14.
//  Copyright (c) 2014 semenag01. All rights reserved.
//

#import "SMOdnoklassniki.h"
#import "NSDictionary+NullProtected.h"

/*NSString * const appId = @"<app id>";
 NSString * const appKey = @"<app key>";
 NSString * const appSecret = @"<secret key>";
 */


@interface SMOdnoklassniki ()<OKSessionDelegate,OKRequestDelegate>
{
    Odnoklassniki *okApi;
    NSArray *permissions;
}
@property (nonatomic, strong, readwrite) Odnoklassniki *okApi;

@end

@implementation SMOdnoklassniki
@synthesize okApi;

SM_IMPLEMENT_SINGLETON;

- (instancetype)init
{
    self = [super init];
    if (self)
    {
    }
    return self;
}

- (void)setup
{
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"SMOdnoklassniki" ofType:@"plist"];
    NSDictionary* config = [NSDictionary dictionaryWithContentsOfFile:filePath];
    
    if(config)
    {
        NSLog(@"Odnoklassniki config %@", config);
        permissions = [config objectForKey:@"permissions"];
        
        NSString *appId = [[config objectForKey:@"appId"] description];
        NSString *appKey = [[config objectForKey:@"appKey"] description];
        NSString *appSecret = [[config objectForKey:@"appSecret"] description];
        okApi = [[Odnoklassniki alloc] initWithAppId:appId andAppSecret:appSecret andAppKey:appKey andDelegate:self];
    }
    if (!permissions)
        permissions = @[@"VALUABLE_ACCESS"];
    
    redirectUrl = [config objectForKey:@"redirect_url"];
    callbacks = [NSMutableDictionary new];
}

- (void)loginWithCallback:(SMSocialNetworkCallback)aCallback
{
    NSParameterAssert(aCallback);

    [self registerCallback:aCallback forEvent:kSocialNetworkCallBackLogin];
    if (okApi.isSessionValid)
    {
        [okApi refreshToken];
    }
    else
    {
        [okApi authorize:permissions];
    }
}

- (void)logout
{
    [super logout];
    [okApi logout];
}

- (NSString *)accessToken
{
    return okApi.session.accessToken;
}

- (BOOL)isSessionValid
{
    return okApi.isSessionValid;
}

- (void)postOnMyWallWithShareItem:(SMSocialShareItem *)aShareItem callback:(SMSocialNetworkCallback)aCallback
{
    NSParameterAssert(aShareItem);
    
    [self registerCallback:aCallback forEvent:kSocialNetworkCallBackPostWall];
    
    void(^block)(void) = ^(void)
    {
        NSMutableDictionary *shareDic = [NSMutableDictionary new];
        
        NSString *link = aShareItem.link;
        NSString *text = aShareItem.message;
        
        [shareDic setNilProtectedObject:link forKey:@"linkUrl"];
        [shareDic setNilProtectedObject:text forKey:@"comment"];
        
        OKRequest *request = [Odnoklassniki requestWithMethodName:@"share.addLink" andParams:shareDic andHttpMethod:@"GET" andDelegate:self];
        //OKRequest *request = [Odnoklassniki requestWithMethodName:@"friends.get" andParams:nil andHttpMethod:@"GET" andDelegate:self];
        [request load];
    };
    
    [self loginWithCallback:^(id data, NSError *error, BOOL canceled)
     {
         if (!error && !canceled)
         {
             block();
         }
         else
         {
             aCallback(nil, error, canceled);
         }
     }];
}

#pragma mark - Application events
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    BOOL result = NO;
    NSString *redirect_Url = [redirectUrl lowercaseString];
    
    if ([[[url absoluteString] lowercaseString] hasPrefix:redirect_Url])
    {
        return [okApi.session handleOpenURL:url];
    }
    return result;
}

#pragma mark - OKSessionDelegate
- (void)okDidLogin
{
    [self performCallBackWithKey:kSocialNetworkCallBackLogin object:okApi.session.accessToken error:nil cancel:NO];
}

- (void)okDidNotLogin:(BOOL)canceled
{
    [self performCallBackWithKey:kSocialNetworkCallBackLogin object:nil error:nil cancel:canceled];
}

- (void)okDidNotLoginWithError:(NSError *)error
{
    [self performCallBackWithKey:kSocialNetworkCallBackLogin object:nil error:error cancel:NO];
}

- (void)okDidExtendToken:(NSString *)accessToken
{
    [self performCallBackWithKey:kSocialNetworkCallBackLogin object:accessToken error:nil cancel:NO];
}

- (void)okDidNotExtendToken:(NSError *)error
{
    [self performCallBackWithKey:kSocialNetworkCallBackLogin object:nil error:error cancel:NO];
}

#pragma mark - OKRequestDelegate
- (void)request:(OKRequest *)request didLoad:(id)result
{
    [self performCallBackWithKey:kSocialNetworkCallBackPostWall object:result error:nil cancel:NO];
}

- (void)request:(OKRequest *)request didFailWithError:(NSError *)error
{
    [self performCallBackWithKey:kSocialNetworkCallBackPostWall object:nil error:error cancel:NO];
}

#pragma mark - Application events
- (void)applicationDidBecomeActive:(NSNotification *)aNotification
{
    [self performCallBackWithKey:kSocialNetworkCallBackLogin object:nil error:nil cancel:YES];
}

- (void)applicationWillTerminate:(NSNotification *)aNotification
{
    [okApi logout];
}

@end
