//
//  SMSocialsTestController.h
//  Semenag01SocialKit
//
//  Created by semenag01 on 2/24/14.
//  Copyright (c) 2014 semenag01. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SMSocialsTestController : UIViewController
{
    IBOutlet UIView* activitiView;
    IBOutlet UIActivityIndicatorView* activitiIndicator;
}

@property (strong, nonatomic) NSString *objectID;

@end
