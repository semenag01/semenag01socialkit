//
//  AppDelegate.m
//  Semenag01SocialKit
//
//  Created by semenag01 on 8/5/15.
//  Copyright (c) 2015 semenag01. All rights reserved.
//

#import "AppDelegate.h"
#import "SMSocialNetwork.h"
#import "SMSocialsTestController.h"
//#=begin#=end
@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = [SMSocialsTestController new];
    [self.window makeKeyAndVisible];
    return YES;
}

- (BOOL)application: (UIApplication *)application openURL: (NSURL *)url sourceApplication: (NSString *)sourceApplication annotation: (id)annotation
{
    return [SMSocialNetwork application:application openURL:url sourceApplication:sourceApplication annotation:application];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    
}

/*
 
 */
@end
