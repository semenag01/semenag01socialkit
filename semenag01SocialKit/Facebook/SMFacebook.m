//
//  SMFacebook.m
//  APITest
//
//  Created by Yuriy Bosov on 2/25/13.
//  Copyright (c) 2013 semenag01. All rights reserved.
//

#import "SMFacebook.h"
//#import "FacebookSDK.h"
#import "NSDictionary+NullProtected.h"
#import "SMUserProfile+Facebook.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>


// Graph Path
#define kGraphPathMe                @"me"
#define kGraphPathMeFeed            @"me/feed"
#define kGraphPathMyFriends         @"me/friends"
#define kGraphPathApprequests       @"apprequests"

@interface SMFacebook()
{
    NSArray* permissions;
    NSString* profileFields;
    FBSDKLoginManager *fbLoginManager;
}

@property(nonatomic, readonly) FBSDKLoginManager *fbLoginManager;;

- (void)setup;

@end


@implementation SMFacebook
@synthesize fbLoginManager;

SM_IMPLEMENT_SINGLETON;



/*
 *Override method
 */
- (void)setup
{
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"SMFacebookConfiguration" ofType:@"plist"];
    NSDictionary* config = [NSDictionary dictionaryWithContentsOfFile:filePath];
    
    if(config)
    {
        NSLog(@"facebook config %@", config);
        permissions = [config objectForKey:@"permissions"];
    }
    if (!permissions)
        permissions = [NSArray arrayWithObjects:@"public_profile",@"email", nil];
    
    redirectUrl = [config objectForKey:@"redirect_url"];
    
    profileFields = [config objectForKey:@"profile_fields"];
    
    if (!profileFields)
    {
        profileFields = @"id,name,email,first_name,last_name,gender,locale,location,birthday,address,picture,photos,hometown,cover,middle_name,name_format";
    }
    
    NSAssert(redirectUrl, @"Need redirect url!");
}

- (NSString*)accessToken
{
    return [FBSDKAccessToken currentAccessToken].tokenString;;
}

#pragma mark - Login, Logout, UserData
- (void)loginWithCallback:(SMSocialNetworkCallback)aCallback
{
    NSParameterAssert(aCallback);
    
    [self.fbLoginManager logInWithReadPermissions:permissions handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        
        aCallback(result.token.tokenString,error,result.isCancelled);
    }];
}

- (void)logout
{
    [super logout];
    [self.fbLoginManager logOut];
}

- (BOOL)isSessionValid
{
//    return [FBSession.activeSession isOpen];
    return (BOOL)[FBSDKAccessToken currentAccessToken];
}

- (void)userProfileWithCallback:(SMSocialNetworkCallback)aCallback
{
    void(^block)(void) = ^(void)
    {
        NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
        [parameters setValue:profileFields forKey:@"fields"];

        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                      id result, NSError *error) {
             if (!error)
             {
                 NSLog(@"fetched user:%@", result);
                 SMUserProfile *profile = [SMUserProfile userWithFaceBookDictinary:result];
                 aCallback(profile,nil,NO);
             } else
             {
                 aCallback(nil,error,NO);
             }
         }];
    };
    
    if (self.isSessionValid)
    {
        block();
    }
    else
    {
        [self loginWithCallback:^(id data, NSError *error, BOOL canceled)
         {
             if (data)
             {
                 block();
             }
             else
             {
                 aCallback(data, error, canceled);
             }
         }];
    }
}

#pragma mark - Friends List
- (void)friendsListWithCallback:(SMSocialNetworkCallback)aCallback
{
    
    //////////
    
    void(^block)(void) = ^(void)
    {
        NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
        [parameters setValue:profileFields forKey:@"fields"];
        
        
        FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                      initWithGraphPath:@"me/taggable_friends"
                                      parameters:parameters
                                      HTTPMethod:@"GET"];
        [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                              id result,
                                              NSError *error) {
            if (!error)
            {
                NSMutableArray *friends = [NSMutableArray new];
                NSArray *friendsDic = [result nullProtectedObjectForKey:@"data"];
                
                for (NSDictionary *dic in friendsDic)
                {
                    SMUserProfile *profile = [SMUserProfile userWithFaceBookDictinary:dic];
                    
                    [friends addObject:profile];
                }
                
                aCallback(friends,nil,NO);
            } else
            {
                aCallback(nil,error,NO);
            }
        }];
    };
    
    if (self.isSessionValid)
    {
        block();
    }
    else
    {
        [self loginWithCallback:^(id data, NSError *error, BOOL canceled)
         {
             if (data)
             {
                 block();
             }
             else
             {
                 aCallback(data, error, canceled);
             }
         }];
    }

}

#pragma mark - Post On My Wall
- (void)postOnMyWallWithData:(NSDictionary *)aPostData callback:(SMSocialNetworkCallback)aCallback
{
    NSParameterAssert(aCallback);
    NSParameterAssert(aPostData);
/*    NSMutableDictionary *postData = [NSMutableDictionary dictionaryWithDictionary:aPostData];
    
    NSString *message = [postData nullProtectedObjectForKey:kFeedMessageKey];
    if (message) {
        [postData removeObjectForKey:kFeedMessageKey];
        [postData setValue:message forKey:kFeedCaptionKey];
    }
    void(^block)(void) = ^(void)
    {
        [FBWebDialogs presentFeedDialogModallyWithSession:[FBSession activeSession] parameters:[FBGraphObject graphObjectWrappingDictionary:postData]
                                                  handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error)
        {

            if (result == FBWebDialogResultDialogCompleted) {
               
                if ([resultURL.absoluteString isEqualToString:@"fbconnect://success"])
                {
                    aCallback(nil, nil, YES);
                }else
                {
                    aCallback(resultURL, nil, NO);
                }
            }else
            {
                if (error) {
                    aCallback(nil, error, NO);
                }else
                {
                    aCallback(nil, nil, YES);
                }
            }

        }];
    };
    
    if ([[FBSession activeSession] isOpen])
    {
        block();
    }
    else
    {
        [self loginWithCallback:^(id data, NSError *error, BOOL canceled)
         {
             if (!error)
             {
                 block();
             }
             else
             {
                 aCallback(nil, error, canceled);
             }
         }];
    }*/
}

- (void)postOnMyWallWithShareItem:(SMSocialShareItem *)aShareItem callback:(SMSocialNetworkCallback)aCallback
{
    
}
- (void)postOnMyWallWithData:(NSDictionary*)aPostData callback:(SMSocialNetworkCallback)aCallback showDialog:(BOOL)aShowDialog
{
    /*
    NSParameterAssert(aPostData);
    NSMutableDictionary *postData = [NSMutableDictionary dictionaryWithDictionary:aPostData];
    
    NSString *message = [postData nullProtectedObjectForKey:kFeedMessageKey];
    if (message) {
        [postData removeObjectForKey:kFeedMessageKey];
        [postData setValue:message forKey:kFeedCaptionKey];
    }
    void(^block)(void) = ^(void)
    {
        if (aShowDialog)
        {
            [FBWebDialogs presentFeedDialogModallyWithSession:[FBSession activeSession] parameters:[FBGraphObject graphObjectWrappingDictionary:postData]
                                                      handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error)
             {
                 [self errorHandler:error];
                 BOOL cancel = (result == FBWebDialogResultDialogNotCompleted  && !resultURL && !error)||
                                (result == FBWebDialogResultDialogCompleted && [resultURL.absoluteString isEqualToString:@"fbconnect://success"]);
                 aCallback(resultURL, error, cancel);
             }];
        }
        else
        {
            
            void(^requestBlock)(void) = ^(void)
            {
                [FBRequestConnection startWithGraphPath:kGraphPathMeFeed parameters:[FBGraphObject graphObjectWrappingDictionary:postData] HTTPMethod:@"POST" completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
                 {
                     [self errorHandler:error];
                     aCallback(result, error, NO);
                 }];
            };
            
            if ([FBSession.activeSession.permissions indexOfObject:@"publish_actions"] == NSNotFound)
            {
                [FBSession.activeSession requestNewPublishPermissions:[NSArray arrayWithObject:@"publish_actions"]
                                                      defaultAudience:FBSessionDefaultAudienceFriends
                                                    completionHandler:^(FBSession *session, NSError *error)
                {
                     if (!error)
                     {
                         requestBlock();
                     }
                    else
                    {
                        aCallback(nil, error, NO);
                    }
                 }];
            }
            else
            {
                requestBlock();
            }
        }
    };
    
    if ([[FBSession activeSession] isOpen])
    {
        block();
    }
    else
    {
        [self loginWithCallback:^(id data, NSError *error, BOOL canceled)
         {
             if (!error)
             {
                 block();
             }
             else
             {
                 aCallback(data, error, canceled);
             }
         }];
    }*/
}

#pragma mark - Send App Invite
- (void)sendInviteToFriendWithData:(NSDictionary *)aPostData callback:(SMSocialNetworkCallback)aCallback
{
    NSParameterAssert(aCallback);
    NSParameterAssert(aPostData);
    
/*    void(^block)(void) = ^(void)
    {
        [FBWebDialogs presentDialogModallyWithSession:[FBSession activeSession]
                                               dialog:kGraphPathApprequests
                                           parameters:aPostData
                                              handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error)
        {
            if (error)
            {
                [self errorHandler:error];
            }
            aCallback(resultURL, error, (result == FBWebDialogResultDialogNotCompleted || [[resultURL absoluteString] hasPrefix: @"fbconnect://success?error_code=4201"]));
        }];
    };
    
    if ([[FBSession activeSession] isOpen])
    {
        block();
    }
    else
    {
        [self loginWithCallback:^(id data, NSError *error, BOOL canceled)
         {
             if (!error)
             {
                 block();
             }
             else
             {
                 aCallback(data, error, canceled);
             }
         }];
    }
*/
}

- (FBSDKLoginManager *)fbLoginManager
{
    if (!fbLoginManager)
    {
        fbLoginManager = [[FBSDKLoginManager alloc] init];
    }
    return fbLoginManager;
}

#pragma mark - Application events
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    BOOL result = NO;
    result = [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
    return result;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    [[FBSDKApplicationDelegate sharedInstance] application:[UIApplication sharedApplication]
                             didFinishLaunchingWithOptions:aNotification.userInfo];
}

- (void)applicationDidBecomeActive:(NSNotification *)aNotification
{
    [FBSDKAppEvents activateApp];
}

@end