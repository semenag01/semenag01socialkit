//
//  SMUserProfile+Facebook.m
//  Semenag01SocialKit
//
//  Created by Yuriy Bosov on 10/20/14.
//  Copyright (c) 2014 semenag01. All rights reserved.
//

#import "SMUserProfile+Facebook.h"
#import "SMKitDefines.h"
#import "NSDictionary+NullProtected.h"
#import "SMUserProfile+Facebook.h"

@implementation SMUserProfile (Facebook)

/*
 * facebook
 */
+ (SMUserProfile*)userWithFaceBookDictinary:(NSDictionary*)aDictionary
{
    SMUserProfile* user = [[SMUserProfile alloc] init];
    [user setupWithFaceBookDictinary:aDictionary];
    return user;
}

- (void)setupWithFaceBookDictinary:(NSDictionary*)aDictionary
{
    self.typeSocialNetwork = SMUserSocialNetworkFacebook;
    self.sourceDictionary = aDictionary;
    self.ID = SM_NULL_PROTECT([aDictionary objectForKey:@"id"]);
    self.email = SM_NULL_PROTECT([aDictionary objectForKey:@"email"]);
    self.firstName = SM_NULL_PROTECT([aDictionary objectForKey:@"first_name"]);
    self.lastName = SM_NULL_PROTECT([aDictionary objectForKey:@"last_name"]);
    self.name = SM_NULL_PROTECT([aDictionary objectForKey:@"name"]);
    if (!self.name && self.name.length) {
        if (!self.firstName) {
            self.firstName = @"";
        }
        if (!self.lastName) {
            self.lastName = @"";
        }
        self.name = [NSString stringWithFormat:@"%@ %@",self.firstName, self.lastName];
    }
    self.login = SM_NULL_PROTECT([aDictionary objectForKey:@"username"]);
    self.gender = SM_NULL_PROTECT([aDictionary objectForKey:@"gender"]);
    
    NSString *locale = SM_NULL_PROTECT([aDictionary objectForKey:@"locale"]);
    
    if (locale)
    {
        NSLocale *loc = [[NSLocale alloc] initWithLocaleIdentifier:locale];
        self.language = [loc objectForKey:NSLocaleLanguageCode];
        self.location = [loc objectForKey:NSLocaleCountryCode];

    }
    
    NSString* avatarStr = SM_NULL_PROTECT([aDictionary valueForKeyPath:@"picture.data.url"]);
    if ([avatarStr length])
    {
        self.avatarURL = [NSURL URLWithString:avatarStr];
    }
    
    NSString* db = SM_NULL_PROTECT([aDictionary objectForKey:@"birthday"]);
    if (db)
    {
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        formatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        formatter.dateFormat = @"MM/dd/yyyy";
        self.birthday = [formatter dateFromString:db];
    }
}
//end facebook

@end
