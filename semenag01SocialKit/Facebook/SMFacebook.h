//
//  SMFacebook.h
//  APITest
//
//  Created by Yuriy Bosov on 2/25/13.
//  Copyright (c) 2013 semenag01. All rights reserved.
//


/*
 FirstPrintShop IOS App
 App ID:	620418584672675
 App Secret:	ff233d207734eb64764dd2b93c719014(сбросить)
 This app is in Sandbox Mode(Only visible to Admins, Developers and Testers)
 */

#import "SMSocialNetwork.h"

// key for create invite dictionary
#define kInviteTitleKey         @"title"
#define kInviteMessageKey       @"message"
#define kInviteToKey            @"to"

#define kFeedCaptionKey         @"caption"      //can only be used if kFeedLinkKey is specified


#define SMFacebookInstance [SMFacebook sharedInstance]

@interface SMFacebook :SMSocialNetwork


SM_DECLARE_SINGLETON;

// configurations:

// post my wall, invite to friends
- (void)postOnMyWallWithData:(NSDictionary*)aPostData callback:(SMSocialNetworkCallback)aCallback showDialog:(BOOL)aShowDialog;
- (void)sendInviteToFriendWithData:(NSDictionary*)aPostData callback:(SMSocialNetworkCallback)aCallback;

@end
