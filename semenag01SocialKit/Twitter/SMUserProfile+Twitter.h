//
//  SMUserProfile+Twitter.h
//  Semenag01SocialKit
//
//  Created by Yuriy Bosov on 10/20/14.
//  Copyright (c) 2014 semenag01. All rights reserved.
//

#import "SMUserProfile.h"

@interface SMUserProfile (Twitter)

+ (SMUserProfile*)userWithTwitterDictinary:(NSDictionary*)aDictionary;
- (void)setupWithTwitterDictionary:(NSDictionary*)aDictionary;

@end
