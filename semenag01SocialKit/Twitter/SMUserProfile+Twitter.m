//
//  SMUserProfile+Twitter.m
//  Semenag01SocialKit
//
//  Created by Yuriy Bosov on 10/20/14.
//  Copyright (c) 2014 semenag01. All rights reserved.
//

#import "SMUserProfile+Twitter.h"
#import "SMKitDefines.h"
#import "NSDictionary+NullProtected.h"


@implementation SMUserProfile (Twitter)

/*
 * twitter
 */
+ (SMUserProfile*)userWithTwitterDictinary:(NSDictionary*)aDictionary
{
    SMUserProfile* user = [[SMUserProfile alloc] init];
    [user setupWithTwitterDictionary:aDictionary];
    return user;
}
- (void)setupWithTwitterDictionary:(NSDictionary*)aDictionary
{
    self.typeSocialNetwork = SMUserSocialNetworkTwitter;
    self.sourceDictionary = aDictionary;
    
    self.ID = [aDictionary nullProtectedObjectForKey:@"id"];
    NSString *name = [aDictionary nullProtectedObjectForKey:@"name"];
    
    self.name = name;
    
    if (name)
    {
        NSRange rangeSp = [name rangeOfString:@" "];
        NSRange range_ = [name rangeOfString:@"_"];
        NSRange range;
        
        if (range_.location == NSNotFound &&
            rangeSp.location == NSNotFound)
        {
            self.firstName = name;
        }
        else
        {
            if (rangeSp.location != NSNotFound)
            {
                range = rangeSp;
            }
            else if (range_.location != NSNotFound)
            {
                range = range_;
            }
            if (range.location != NSNotFound)
            {
                NSRange rangeByFirst = NSMakeRange(0, range.location);
                NSRange rangeByLast = NSMakeRange(range.location+1, name.length - range.location-1);
                
                self.firstName = [name substringWithRange:rangeByFirst];
                
                self.lastName = [name substringWithRange:rangeByLast];
            }
            else
            {
                self.firstName = name;
            }
        }
    }
    self.login = [aDictionary nullProtectedObjectForKey:@"screen_name"];
    self.email = [aDictionary nullProtectedObjectForKey:@"email"];
    self.language = [aDictionary nullProtectedObjectForKey:@"lang"];
    
    NSString *loc = [aDictionary nullProtectedObjectForKey:@"location"];
    self.location = [self getCountryCodeByAddress:loc];
    
    NSString* urlString = [aDictionary nullProtectedObjectForKeyPath:@"profile_image_url"];
    if ([urlString length])
        self.avatarURL = [NSURL URLWithString:urlString];
    
}
//end twitter
@end
