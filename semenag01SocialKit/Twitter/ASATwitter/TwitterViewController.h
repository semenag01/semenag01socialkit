//
//  ViewController.h
//  SocialServicesProject
//
//  Created by AndrewShmig on 04/20/13.
//  Copyright (c) 2013 AndrewShmig. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASATwitterCommunicator.h"
#import "SMTwitter.h"

@interface TwitterViewController : UIViewController
{
    SMSocialNetworkCallback callback;
}

@property UIWebView *webView;
@property ASATwitterCommunicator *tw;

- (id)initWithCallBack:(SMSocialNetworkCallback)aCallback;

@end