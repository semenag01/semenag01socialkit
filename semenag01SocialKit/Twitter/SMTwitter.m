//
//  SMTwitter.m
//  OutSpeak
//
//  Created by Alexander Burkhai on 9/27/13.
//  Copyright (c) 2013 semenag01. All rights reserved.
//

#import "SMTwitter.h"
#import "TwitterViewController.h"
#import "SMUserProfile+Twitter.h"
#import "ASATwitterUserAccount.h"

#define kSMTwitterOAuthTokenKey                 @"kSMTwitterOAuthTokenKey"
#define kSMTwitterOAuthTokenSecretKey           @"kSMTwitterOAuthTokenSecretKey"
#define kSMTwitterUserIDKey                     @"kSMTwitterUserIDKey"
#define kSMTwitterScreenName                    @"kSMTwitterScreenName"

@interface SMTwitter ()
{
    NSString *consumerKey;
    NSString *consumerSecret;
    
    ASATwitterUserAccount *account;
}

@end

@implementation SMTwitter

SM_IMPLEMENT_SINGLETON;

@synthesize oauthToken, oauthTokenSecret;
@synthesize consumerKey,consumerSecret;
@synthesize viewControllerFromPressent;

- (void)setup
{
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"SMTwitterConfiguration" ofType:@"plist"];
    NSDictionary* config = [NSDictionary dictionaryWithContentsOfFile:filePath];
    
    SMLog(@"twitter config %@", config);
    consumerKey = [config valueForKey:@"consumer_key"];
    consumerSecret = [config valueForKey:@"consumer_secret"];
    
    NSParameterAssert(consumerKey);
    NSParameterAssert(consumerSecret);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    account = [[ASATwitterUserAccount alloc] initWithToken:[defaults objectForKey:kSMTwitterOAuthTokenKey]
                                               tokenSecret:[defaults objectForKey:kSMTwitterOAuthTokenSecretKey]
                                             twitterUserID:[defaults objectForKey:kSMTwitterUserIDKey]
                                            userScreenName:[defaults objectForKey:kSMTwitterScreenName]];
    if (![self isSessionValid])
        account = nil;
}

- (BOOL)isSessionValid
{
    return
    account.oauthToken.length &&
    account.oauthTokenSecret.length &&
    account.twitterUserID.length &&
    account.screenName.length;
}

- (NSString *)oauthToken
{
    return account.oauthToken;
}

- (NSString *)oauthTokenSecret
{
    return account.oauthTokenSecret;
}

- (void)loginWithCallback:(SMSocialNetworkCallback)aCallback
{
    NSParameterAssert(aCallback);
    __weak __typeof(&*self) weakSelf = self;
    SMSocialNetworkCallback block = ^(id data, NSError* error, BOOL canceled)
    {
        if (data && !error && !canceled)
        {
            if ([data isKindOfClass:[ASATwitterUserAccount class]])
            {
                [weakSelf setupAccount:data];
            }
        }
        
        aCallback(data,error,canceled);
    };
    
    if ([self isSessionValid])
    {
        aCallback(account, nil, NO);
    }
    else
    {
        TwitterViewController *vc = [[TwitterViewController alloc] initWithCallBack:block];
        if (SM_IS_IPAD)
        {
            vc.modalPresentationStyle = UIModalPresentationFormSheet;
            //vc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        }
        
        [self.viewControllerFromPressent presentViewController:vc animated:YES completion:nil];
    }
}

#pragma mark - Logout

- (void)clearAllTwitterCookies {
    
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (NSHTTPCookie *cookie in [cookieStorage cookies])
    {
        if ([[cookie domain] rangeOfString:@"twitter.com"].location != NSNotFound) {
            [cookieStorage deleteCookie:cookie];
        }
    }
}

- (void)clearAccessToken
{
    account = nil;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:kSMTwitterOAuthTokenKey];
    [defaults removeObjectForKey:kSMTwitterOAuthTokenSecretKey];
    [defaults removeObjectForKey:kSMTwitterUserIDKey];
    [defaults removeObjectForKey:kSMTwitterScreenName];
    [defaults synchronize];
}

- (void)logout
{
    [super logout];
    twitterAPI = nil;
    [self clearAllTwitterCookies];
    [self clearAccessToken];
}

- (void)setupAccount:(ASATwitterUserAccount*)anAccount
{
    account = anAccount;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:account.oauthToken forKey:kSMTwitterOAuthTokenKey];
    [defaults setObject:account.oauthTokenSecret forKey:kSMTwitterOAuthTokenSecretKey];
    [defaults setObject:account.twitterUserID forKey:kSMTwitterUserIDKey];
    [defaults setObject:account.screenName forKey:kSMTwitterScreenName];
    
    [defaults synchronize];
}

#pragma mark - UserProfile

- (void)userProfileWithCallback:(SMSocialNetworkCallback)aCallback
{
    assert(aCallback);
    void(^block)(ASATwitterUserAccount *) = ^(ASATwitterUserAccount *anAccount)
    {
        //[self setupAccount:anAccount];
        
        if (!twitterAPI)
            twitterAPI = [STTwitterAPI twitterAPIWithOAuthConsumerKey:consumerKey consumerSecret:consumerSecret oauthToken:anAccount.oauthToken oauthTokenSecret:anAccount.oauthTokenSecret];
        
        [twitterAPI getUsersShowForUserID:anAccount.twitterUserID orScreenName:anAccount.screenName includeEntities:@(NO) successBlock:^(NSDictionary *userDictionary)
         {
             SMUserProfile *user = [SMUserProfile userWithTwitterDictinary:userDictionary];
             aCallback(user, nil, NO);
         }
                               errorBlock:^(NSError *error)
         {
             aCallback(nil, error, NO);
         }];
    };
    
    if ([self isSessionValid])
    {
        block(account);
    }
    else
    {
        [self loginWithCallback:^(id data, NSError *error, BOOL cancelled)
         {
             if (!error && !cancelled)
             {
                 block(data);
             }
             else
             {
                 aCallback(data, error, cancelled);
             }
         }];
    }
    
}

- (void)friendsListWithCallback:(SMSocialNetworkCallback)aCallback;
{
    assert(aCallback);
    void(^block)(ASATwitterUserAccount *) = ^(ASATwitterUserAccount *anAccount)
    {
        //[self setupAccount:anAccount];
        
        if (!twitterAPI)
            twitterAPI = [STTwitterAPI twitterAPIWithOAuthConsumerKey:consumerKey consumerSecret:consumerSecret oauthToken:anAccount.oauthToken oauthTokenSecret:anAccount.oauthTokenSecret];
        
        [twitterAPI getFollowersListForUserID:anAccount.twitterUserID
                                 orScreenName:anAccount.screenName
                                        count:@"10000"
                                       cursor:nil
                                   skipStatus:nil
                          includeUserEntities:nil
                                 successBlock:^(NSArray *users, NSString *previousCursor, NSString *nextCursor) {
                                     if (users.count) {
                                         NSMutableArray *models = [NSMutableArray new];
                                         for (NSDictionary *dic in users)
                                         {
                                             SMUserProfile *p = [SMUserProfile userWithTwitterDictinary:dic];
                                             [models addObject:p];
                                         }
                                         aCallback(models, nil, NO);
                                     }else
                                     {
                                         aCallback(nil, nil, NO);
                                     }
                                     
                                 } errorBlock:^(NSError *error) {
                                     aCallback(nil, error, NO);
                                 }];    };
    
    if ([self isSessionValid])
    {
        block(account);
    }
    else
    {
        [self loginWithCallback:^(id data, NSError *error, BOOL cancelled)
         {
             if (!error && !cancelled)
             {
                 block(data);
             }
             else
             {
                 aCallback(data, error, cancelled);
             }
         }];
    }
    
}

- (void)postOnMyWallWithShareItem:(SMSocialShareItem *)aShareItem callback:(SMSocialNetworkCallback)aCallback
{
    NSParameterAssert(aShareItem);

    void(^block)(ASATwitterUserAccount *) = ^(ASATwitterUserAccount *anAccount)
    {
        //[self setupAccount:anAccount];
        
        NSString *message = aShareItem.message;
        NSString *imageStr = aShareItem.imagePath;
        NSString *link = aShareItem.link;
        UIImage *image = aShareItem.image;
        
        if (!twitterAPI)
            twitterAPI = [STTwitterAPI twitterAPIWithOAuthConsumerKey:consumerKey
                                                       consumerSecret:consumerSecret
                                                           oauthToken:anAccount.oauthToken
                                                     oauthTokenSecret:anAccount.oauthTokenSecret];
        NSMutableString *status = [NSMutableString stringWithString:@""];
        if (message) {
            status = [NSMutableString stringWithString:[message stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
        }
        if (link)
        {
            if ([status length])
                [status appendString:@"\n"];
            
            [status appendString:[link stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        }
        NSURL *urlImage = [NSURL URLWithString:imageStr];
        if (urlImage || image)
        {
            if (urlImage) {
                [twitterAPI postStatusUpdate:status
                           inReplyToStatusID:nil
                                    mediaURL:urlImage
                                     placeID:nil
                                    latitude:nil
                                   longitude:nil
                         uploadProgressBlock:nil
                                successBlock:^(NSDictionary *status) {
                                    if (aCallback)
                                        aCallback(status, nil, NO);
                                } errorBlock:^(NSError *error) {
                                    if (aCallback)
                                        aCallback(nil, error, NO);
                                }];
            }else
            {
                [twitterAPI postStatusUpdate:status mediaDataArray:[NSArray arrayWithObject:UIImagePNGRepresentation(image)] possiblySensitive:nil inReplyToStatusID:nil latitude:nil longitude:nil placeID:nil displayCoordinates:nil uploadProgressBlock:nil successBlock:^(NSDictionary *status) {
                    if (aCallback)
                        aCallback(status, nil, NO);
                } errorBlock:^(NSError *error) {
                    if (aCallback)
                        aCallback(nil, error, NO);
                }];
            }
            
        }else
        {
            [twitterAPI postStatusUpdate:status
                       inReplyToStatusID:nil
                                latitude:nil
                               longitude:nil
                                 placeID:nil
                      displayCoordinates:@(NO)
                                trimUser:@(NO)
                            successBlock:^(NSDictionary *status)
             {
                 if (aCallback)
                     aCallback(status, nil, NO);
             } errorBlock:^(NSError *error)
             {
                 if (aCallback)
                     aCallback(nil, error, NO);
             }];
        }
        
        
        
    };
    
    if ([self isSessionValid])
    {
        block(account);
    }
    else
    {
        [self loginWithCallback:^(id data, NSError *error, BOOL cancelled)
         {
             if (!error && !cancelled)
             {
                 block(data);
             }
             else
             {
                 aCallback(data, error, cancelled);
             }
         }];
    }
}

//postDirectMessage

- (void)sendInviteToFriend:(NSString*)aUserLogin withShareItem:(SMSocialShareItem *)aShareItem callback:(SMSocialNetworkCallback)aCallback
{
    void(^block)(ASATwitterUserAccount *) = ^(ASATwitterUserAccount *anAccount)
    {
        //[self setupAccount:anAccount];
        
        NSString *message = aShareItem.message;
        NSString *link = aShareItem.link;
        
        if (!twitterAPI)
            twitterAPI = [STTwitterAPI twitterAPIWithOAuthConsumerKey:consumerKey
                                                       consumerSecret:consumerSecret
                                                           oauthToken:anAccount.oauthToken
                                                     oauthTokenSecret:anAccount.oauthTokenSecret];
        NSMutableString *status = [NSMutableString stringWithString:@""];
        if (message) {
            status = [NSMutableString stringWithString:[message stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
        }
        if (link)
        {
            if ([status length])
                [status appendString:@"\n"];
            
            [status appendString:[link stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        }
        
        [twitterAPI postDirectMessage:status forScreenName:aUserLogin orUserID:nil successBlock:^(NSDictionary *message)
        {
            aCallback(message,nil,NO);
            
        } errorBlock:^(NSError *error)
        {
            aCallback(nil,error,NO);
        }];
    };
    
    if ([self isSessionValid])
    {
        block(account);
    }
    else
    {
        [self loginWithCallback:^(id data, NSError *error, BOOL cancelled)
         {
             if (!error && !cancelled)
             {
                 block(data);
             }
             else
             {
                 aCallback(data, error, cancelled);
             }
         }];
    }
}
@end
