//
//  SMTwitter.h
//  OutSpeak
//
//  Created by Alexander Burkhai on 9/27/13.
//  Copyright (c) 2013 semenag01. All rights reserved.
//
/*
 
 Access level	 Read-only
 About the application permission model
 Consumer key	ryICTN5RsFFNtRZ89n6vpQ
 Consumer secret	EaVsnUkjrKkfkK07DCqTfegfV4Fvy41Oh8TKekX4Cw
 Request token URL	https://api.twitter.com/oauth/request_token
 Authorize URL	https://api.twitter.com/oauth/authorize
 Access token URL	https://api.twitter.com/oauth/access_token
 Callback URL	None
 Sign in with Twitter	No

 */
#import "SMCore.h"

#import "STTwitter.h"
#import "ASATwitterCommunicator.h"
#import "SMSocialNetwork.h"

#define SMTwitterInstance [SMTwitter sharedInstance]

@interface SMTwitter : SMSocialNetwork
{
    STTwitterAPI *twitterAPI;
}

SM_DECLARE_SINGLETON;

@property (nonatomic, readonly) NSString *oauthToken;
@property (nonatomic, readonly) NSString *oauthTokenSecret;

@property (nonatomic, readonly) NSString *consumerKey;
@property (nonatomic, readonly) NSString *consumerSecret;


- (void)sendInviteToFriend:(NSString*)aUserLogin withShareItem:(SMSocialShareItem *)aShareItem callback:(SMSocialNetworkCallback)aCallback;


@end
