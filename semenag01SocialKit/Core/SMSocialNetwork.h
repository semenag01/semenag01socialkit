//
//  SMSocialNetwork.h
//  FirstPrintShop
//
//  Created by Semen on 21.11.13.
//  Copyright (c) 2013 semenag01. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SMUserProfile.h"
#import "SMSingleton.h"
#import "SMKitDefines.h"
#import "SMSocialShareItem.h"

#define kSocialNetworkCallBackLogin          @"kSocialNetworkCallBackLogin"
#define kSocialNetworkCallBackUserData       @"kSocialNetworkCallBackUserData"
#define kSocialNetworkCallBackPostWall       @"kSocialNetworkCallBackPostWall"
#define kSocialNetworkCallBackFriends        @"kSocialNetworkCallBackFriends"

#define kFeedNameKey            @"name"         //can only be used if kFeedLinkKey is specified
#define kFeedDescriptionKey     @"description"  //can only be used if kFeedLinkKey is specified


typedef void (^SMSocialNetworkCallback)(id data, NSError* error, BOOL canceled);

@interface SMSocialNetwork : NSObject
{
    NSString *redirectUrl;
    NSMutableDictionary* callbacks;
}

@property (nonatomic, weak) UIViewController* viewControllerFromPressent;

@property (nonatomic, readonly)  NSString *redirectUrl;
@property (nonatomic, readonly)  NSString *accessToken;

- (void)registerCallback:(SMSocialNetworkCallback)aCallback forEvent:(NSString*)anEvent;
- (void)performCallBackWithKey:(NSString *)aKey object:(id)aObject error:(NSError *)aError cancel:(BOOL)aCancel;

// login\logout
- (void)loginWithCallback:(SMSocialNetworkCallback)aCallback;
- (void)logout;
- (BOOL)isSessionValid;


// user profile
- (void)userProfileWithCallback:(SMSocialNetworkCallback)aCallback;
- (void)postOnMyWallWithShareItem:(SMSocialShareItem *)aShareItem callback:(SMSocialNetworkCallback)aCallback;
- (void)friendsListWithCallback:(SMSocialNetworkCallback)aCallback;

// default use this method in app delegate
+ (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation;

// Override this method in subclasses
- (void)applicationDidFinishLaunching:(NSNotification *)aNotification;
- (void)applicationDidBecomeActive:(NSNotification *)aNotification;
- (void)applicationWillTerminate:(NSNotification *)aNotification;
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation;



@end
