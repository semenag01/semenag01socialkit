//
//  SMSocialShareItem.h
//  Semenag01SocialKit
//
//  Created by semenag01 on 8/13/15.
//  Copyright (c) 2015 semenag01. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SMSocialShareItem : NSObject

@property(nonatomic,strong) NSString *message;
@property(nonatomic,strong) NSString *link;
@property(nonatomic,strong) NSString *imagePath;
@property(nonatomic,strong) UIImage *image;

+ (instancetype)makeWithMessage:(NSString *)aMessage
                           link:(NSString *)aLink
                      imagePath:(NSString *)aImagePath
                          image:(UIImage *)aImage;
@end
