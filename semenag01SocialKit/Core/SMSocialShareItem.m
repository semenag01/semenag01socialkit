//
//  SMSocialShareItem.m
//  Semenag01SocialKit
//
//  Created by semenag01 on 8/13/15.
//  Copyright (c) 2015 semenag01. All rights reserved.
//

#import "SMSocialShareItem.h"

@implementation SMSocialShareItem

+ (instancetype)makeWithMessage:(NSString *)aMessage
                           link:(NSString *)aLink
                      imagePath:(NSString *)aImagePath
                          image:(UIImage *)aImage
{
    SMSocialShareItem *result = [SMSocialShareItem new];
    
    result.message = aMessage;
    result.link = aLink;
    result.imagePath = aImagePath;
    result.image = aImage;
    
    return result;
}

@end
