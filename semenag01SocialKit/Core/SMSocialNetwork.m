//
//  CTSocialNetwork.m
//  FirstPrintShop
//
//  Created by Semen on 21.11.13.
//  Copyright (c) 2013 semenag01. All rights reserved.
//

#import "SMSocialNetwork.h"

@interface SMSocialNetwork()
{
}

@end

@implementation SMSocialNetwork
@synthesize redirectUrl;

static NSMutableArray *instansesForRedirectUrl;


- (id)init
{
    self = [super init];
    if (self)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidFinishLaunching:) name:UIApplicationDidFinishLaunchingNotification object:nil];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillTerminate:) name:UIApplicationWillTerminateNotification object:nil];


        callbacks = [NSMutableDictionary new];
        if (!instansesForRedirectUrl)
        {
            instansesForRedirectUrl = [NSMutableArray new];
        }
        
        [self setup];
        
        if (redirectUrl)
        {
            [instansesForRedirectUrl addObject:self];
        }
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
/*
 * need to override this methods
 */
- (void)setup
{
    NSAssert(nil, @"Override this method in subclasses!");
}

- (NSString*)accessToken
{
    NSAssert(nil, @"Override this method in subclasses!");
    return @"";
}

// login\logout
- (void)loginWithCallback:(SMSocialNetworkCallback)aCallback
{
    NSAssert(nil, @"Override this method in subclasses!");
}

- (void)logout
{
    [callbacks removeAllObjects];
}

- (BOOL)isSessionValid
{
    NSAssert(nil, @"Override this method in subclasses!");
    return NO;
}

// user profile
- (void)userProfileWithCallback:(SMSocialNetworkCallback)aCallback
{
    NSAssert(nil, @"Override this method in subclasses!");
}

- (void)postOnMyWallWithShareItem:(SMSocialShareItem *)aShareItem callback:(SMSocialNetworkCallback)aCallback
{
    NSAssert(nil, @"Override this method in subclasses!");
}

- (void)friendsListWithCallback:(SMSocialNetworkCallback)aCallback
{
    NSAssert(nil, @"Override this method in subclasses!");
}

+ (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    for (SMSocialNetwork *instans in instansesForRedirectUrl)
    {
        NSString *redU = [instans.redirectUrl lowercaseString];
        NSString *urlU = [url.absoluteString lowercaseString];

        if ([urlU hasPrefix:redU])
        {
            return [instans application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
        }
    }
    return NO;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    NSAssert(nil, @"Override this method in subclasses!");
    return YES;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    //Override this method in subclasses
}

- (void)applicationDidBecomeActive:(NSNotification *)aNotification
{
    //Override this method in subclasses
}

- (void)applicationWillTerminate:(NSNotification *)aNotification
{
    //Override this method in subclasses
}

- (void)registerCallback:(SMSocialNetworkCallback)aCallback forEvent:(NSString*)anEvent
{
    if (aCallback)
    {
        [callbacks setObject:[aCallback copy] forKey:anEvent];
    }
}

- (void)performCallBackWithKey:(NSString *)aKey object:(id)aObject error:(NSError *)aError cancel:(BOOL)aCancel
{
    SMSocialNetworkCallback callback = [callbacks objectForKey:aKey];
    if (callback)
    {
        callback(aObject, aError, aCancel);
        [callbacks removeObjectForKey:aKey];
    }
}

@end
