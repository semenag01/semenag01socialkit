//
//  SMUserProfile+GooglePlus.h
//  Semenag01SocialKit
//
//  Created by Yuriy Bosov on 10/20/14.
//  Copyright (c) 2014 semenag01. All rights reserved.
//

#import "SMUserProfile.h"

@class GPPSignIn;

@interface SMUserProfile (GooglePlus)

+ (SMUserProfile*)userWithGooglePlusGPPSignIn:(GPPSignIn*)signIn;
- (void)setupWithGooglePlusGPPSignIn:(GPPSignIn*)signIn;

@end
