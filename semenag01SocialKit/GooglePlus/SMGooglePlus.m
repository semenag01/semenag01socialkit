//
//  MUGooglePlus.m
//  FirstPrintShop
//
//  Created by Semen on 20.11.13.
//  Copyright (c) 2013 semenag01. All rights reserved.
//

#import "SMGooglePlus.h"
#import <GooglePlus/GooglePlus.h>
#import <GoogleOpenSource/GoogleOpenSource.h>
#import "NSDictionary+NullProtected.h"
#import "SMUserProfile+GooglePlus.h"

@interface SMGooglePlus() <GPPSignInDelegate, GPPShareDelegate>
{
    GPPSignIn *googlePlusSignIn;
}


@end

@implementation SMGooglePlus

SM_IMPLEMENT_SINGLETON;



/*
 *Override method
 */
- (void)setup
{
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"SMGooglePlusConfiguration" ofType:@"plist"];
    NSDictionary* config = [NSDictionary dictionaryWithContentsOfFile:filePath];
    NSAssert(config, @"Need config file!");
    [[GPPShare sharedInstance] setDelegate:self];

    redirectUrl = [config objectForKey:@"redirect_url"];
    NSAssert(redirectUrl, @"Need redirect url!");
    
    NSString* clientId = [config objectForKey:@"clientId"];

    googlePlusSignIn = [GPPSignIn sharedInstance];
    
    googlePlusSignIn.shouldFetchGooglePlusUser = YES;
    googlePlusSignIn.shouldFetchGoogleUserEmail = YES;
    googlePlusSignIn.shouldFetchGoogleUserID = YES;

    
    googlePlusSignIn.clientID = clientId;
    googlePlusSignIn.scopes = [NSArray arrayWithObjects: kGTLAuthScopePlusLogin, nil];
    
    googlePlusSignIn.delegate = self;
}

- (NSString*)accessToken
{
    return googlePlusSignIn.idToken;
}

#pragma mark - Login\Logout
- (void)loginWithCallback:(SMSocialNetworkCallback)aCallback
{
    NSParameterAssert(aCallback);
    if (![callbacks objectForKey:kSocialNetworkCallBackLogin])
    {
        if ([googlePlusSignIn.authentication accessToken])
        {
            aCallback([googlePlusSignIn idToken], nil, NO);
        }
        else
        {
            [self registerCallback:aCallback forEvent:kSocialNetworkCallBackLogin];
            [googlePlusSignIn authenticate];
        }
    }
}

- (void)logout
{
    [super logout];
    [googlePlusSignIn signOut];
}

- (BOOL)isSessionValid
{
    return [googlePlusSignIn trySilentAuthentication] && googlePlusSignIn.googlePlusUser;
}

#pragma mark - User profile
- (void)userProfileWithCallback:(SMSocialNetworkCallback)aCallback
{
    NSParameterAssert(aCallback);
    
    [self registerCallback:aCallback forEvent:kSocialNetworkCallBackUserData];

    SMWeakSelf;
    
    void(^block)(void) = ^(void)
    {
        SMUserProfile *user = [SMUserProfile userWithGooglePlusGPPSignIn:googlePlusSignIn];
        
        [weakSelf performCallBackWithKey:kSocialNetworkCallBackUserData object:user error:nil cancel:NO];
    };
    
    if ([googlePlusSignIn trySilentAuthentication] && googlePlusSignIn.googlePlusUser)
    {
        block();
    }
    else
    {
        [self loginWithCallback:^(id data, NSError *error, BOOL canceled)
         {
             if (data)
             {
                 block();
             }
             else
             {
                 [weakSelf performCallBackWithKey:kSocialNetworkCallBackUserData object:nil error:error cancel:error == nil];
             }
         }];
    }
}

#pragma mark - google plus delegate
- (void)finishedWithAuth:(GTMOAuth2Authentication *)auth
                   error:(NSError *)error
{
    [self performCallBackWithKey:kSocialNetworkCallBackLogin object:auth.accessToken error:error cancel:!error];
}

- (void)didDisconnectWithError:(NSError *)error
{
    [self performCallBackWithKey:kSocialNetworkCallBackLogin object:nil error:error cancel:NO];
}

- (void)postOnMyWallWithShareItem:(SMSocialShareItem *)aShareItem callback:(SMSocialNetworkCallback)aCallback
{

    NSParameterAssert(aShareItem);
    
    SMWeakSelf;
    
    [self registerCallback:aCallback forEvent:kSocialNetworkCallBackPostWall];
    
    void(^block)(void) = ^(void)
    {
        [GPPShare sharedInstance].delegate = weakSelf;
        NSString *message = aShareItem.message;
        NSString *imageStr = aShareItem.imagePath;
        NSString *link = aShareItem.link;
        UIImage *image = aShareItem.image;
        
        if (!message)
        {
            message = @"";
        }
        id<GPPNativeShareBuilder> shareBuilder = [[GPPShare sharedInstance] nativeShareDialog];
        if (link && !imageStr)
        {
            
            [shareBuilder setPrefillText:message];
            [shareBuilder setURLToShare:[NSURL URLWithString:link]];
        }
        if (imageStr || image)
        {
            NSString *shareText = (link)?[NSString stringWithFormat:@"%@\n%@",link,message]:message;
            [shareBuilder setPrefillText:shareText];
            NSData *dataImage = nil;
            if (imageStr) {
                dataImage = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageStr]];
            }
            else
            {
                dataImage = UIImagePNGRepresentation(image);
            }
            [shareBuilder attachImageData:dataImage];
        }
        
        [shareBuilder open];
        
        
    };
    
    if ([googlePlusSignIn trySilentAuthentication] && googlePlusSignIn.googlePlusUser)
    {
        block();
    }
    else
    {
        [self loginWithCallback:^(id data, NSError *error, BOOL canceled)
         {
             if (!error)
             {
                 block();
             }
             else
             {
                 aCallback(nil, error, canceled);
             }
         }];
    }
}

#pragma mark - Protocol Delegate
- (void)finishedSharingWithError:(NSError *)error
{
    [self performCallBackWithKey:kSocialNetworkCallBackPostWall object:nil error:error cancel:NO];
}

- (void)finishedSharing:(BOOL)shared
{
    [self performCallBackWithKey:kSocialNetworkCallBackPostWall object:nil error:nil cancel:NO];
}

#pragma mark - Application events
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    BOOL result = NO;
    NSString *edirect_Url = [redirectUrl lowercaseString];
    
    if ([[[url absoluteString] lowercaseString] hasPrefix:edirect_Url])
    {
        result = [GPPURLHandler handleURL:url
                        sourceApplication:sourceApplication
                               annotation:annotation];
        
    }
    return result;
}

- (void)applicationDidBecomeActive:(NSNotification *)aNotification
{
    [self performCallBackWithKey:kSocialNetworkCallBackLogin object:nil error:nil cancel:YES];
}

@end

