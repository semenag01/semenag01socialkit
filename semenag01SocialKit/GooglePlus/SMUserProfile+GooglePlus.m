//
//  SMUserProfile+GooglePlus.m
//  Semenag01SocialKit
//
//  Created by Yuriy Bosov on 10/20/14.
//  Copyright (c) 2014 semenag01. All rights reserved.
//

#import "SMUserProfile+GooglePlus.h"

#import <GooglePlus/GooglePlus.h>
#import <GoogleOpenSource/GoogleOpenSource.h>
#import "SMKitDefines.h"
#import "NSDictionary+NullProtected.h"


@implementation SMUserProfile (GooglePlus)

/*
 * google
 */
+ (SMUserProfile*)userWithGooglePlusGPPSignIn:(GPPSignIn*)signIn
{
    SMUserProfile* user = [[SMUserProfile alloc] init];
    [user setupWithGooglePlusGPPSignIn:signIn];
    
    return user;
}

- (void)setupWithGooglePlusGPPSignIn:(GPPSignIn*)signIn
{
    self.typeSocialNetwork = SMUserSocialNetworkGooglePlus;
    
    self.ID = SM_NULL_PROTECT(signIn.googlePlusUser.identifier);//
    self.email = SM_NULL_PROTECT(signIn.userEmail);//
    if (!self.email)
    {
        self.email = signIn.authentication.userEmail;
    }
    self.firstName = SM_NULL_PROTECT(signIn.googlePlusUser.name.givenName);//
    self.lastName = SM_NULL_PROTECT(signIn.googlePlusUser.name.familyName);//
    self.name = SM_NULL_PROTECT(signIn.googlePlusUser.displayName);
    if (!self.name && self.name.length) {
        if (!self.firstName) {
            self.firstName = @"";
        }
        if (!self.lastName) {
            self.lastName = @"";
        }
        self.name = [NSString stringWithFormat:@"%@ %@",self.firstName, self.lastName];
    }
    self.login = SM_NULL_PROTECT(signIn.googlePlusUser.nickname);//
    self.gender = SM_NULL_PROTECT(signIn.googlePlusUser.gender);//
    self.language = SM_NULL_PROTECT(signIn.googlePlusUser.language);//
    self.location = SM_NULL_PROTECT(signIn.googlePlusUser.currentLocation);//
    
    NSString* avatarStr = SM_NULL_PROTECT(signIn.googlePlusUser.image.url);
    
    if ([avatarStr length])
    {
        self.avatarURL = [NSURL URLWithString:avatarStr];
    }
    
    NSString* db = [signIn.googlePlusUser birthday];//
    if (db)
    {
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        formatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        formatter.dateFormat = @"yyyy-MM-dd";
        self.birthday = [formatter dateFromString:db];
    }
}
//end google

@end
