//command for clean local pods
sudo rm -fr ~/Library/Caches/CocoaPods/
sudo rm -fr ~/.cocoapods/repos/master

pod lib lint Semenag01SocialKit.podspec
pod trunk register semenag01@meta.ua 'semenag01' --description='macbook pro 15'
pod trunk push Semenag01SocialKit.podspec --allow-warnings --use-libraries
pod spec lint Semenag01SocialKit.podspec --use-libraries
